set -e

SCRIPT_BASEDIR=$(realpath "$(dirname "$0")")

PROJECT_DIR=$(realpath "$SCRIPT_BASEDIR/../../")
UT_PATH="$PROJECT_DIR/test/ut"

run_test() {
    echo "Start to run test."
    cd "$PROJECT_DIR" || exit
    pytest "$ST_PATH"
    echo "Test all use cases success."
}

run_test
